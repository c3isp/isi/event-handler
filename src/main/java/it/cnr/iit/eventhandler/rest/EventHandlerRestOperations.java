package it.cnr.iit.eventhandler.rest;

public class EventHandlerRestOperations {
	public static final String EVENTS = "/v1/events";
	public static final String NOTIFY_EVENT = "/v1/notifyEvent";
	public static final String SUBSCRIBE = "/v1/subscribe";
	public static final String SUBSCRIBE_EV = "/v1/subscribe_ev";
	public static final String SUBSCRIBERS = "/v1/subscribers";
	public static final String UNSUBSCRIBE = "/v1/unsubscribe";

	private EventHandlerRestOperations() {
	}
}
