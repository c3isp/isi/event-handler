package it.cnr.iit.eventhandler;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.SubscribeEvent;
import it.cnr.iit.common.utility.RESTUtils;

public class EventRecord {

	String eventType;
	private Event event = null;
	private final Map<String, SubscribeEvent> subscribersMap;

	public EventRecord(RequestEvent event) {
		this.event = event;
		eventType = event.getEventType();
		subscribersMap = new HashMap<>();
	}

	public EventRecord(String eventType) {
		this.eventType = eventType;
		subscribersMap = new HashMap<>();
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Map<String, SubscribeEvent> getSubscribersMap() {
		return subscribersMap;
	}

	public boolean addSubscriber(SubscribeEvent SubscribeEvent) {
		Optional<URL> url = RESTUtils.parseUrl(SubscribeEvent.getUrl());
		url.ifPresent(u -> subscribersMap.put(u.toString(), SubscribeEvent));
		return url.isPresent();
	}

	public boolean removeSubscriber(SubscribeEvent event) {
		return subscribersMap.remove(event.getUrl()) != null;
	}

	public boolean hasSubscriber(String url) {
		return subscribersMap.values().stream().anyMatch(s -> s.getUrl().equals(url));
	}

}
