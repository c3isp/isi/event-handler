package it.cnr.iit.eventhandler.rest;

import java.util.logging.Logger;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;

@ApiModel(value = "EventHandlerTest", description = "event handler test")
@RestController
public class EventHandlerTestController {

	private static final Logger LOG = Logger.getLogger(EventHandlerTestController.class.getName());

	@PostMapping("/v1/collect")
	public ResponseEvent collect(@RequestBody MultiResourceHandlerEvent event) {
		LOG.info("[TEST] EventReceived : " + event.getEventType() + " : " + event.getSessionID());
		return new ResponseEvent(true);
	}

	@PostMapping("/v1/collect2")
	public ResponseEvent collect2(@RequestBody MultiResourceHandlerEvent event) {
		LOG.info("[TEST] EventReceived2 : " + event.getEventType() + " : " + event.getSessionID());
		return new ResponseEvent(true);
	}

}
