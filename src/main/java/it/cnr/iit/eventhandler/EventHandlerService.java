package it.cnr.iit.eventhandler;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.SubscribeEvent;
import it.cnr.iit.common.utility.RESTUtils;

@Service
public class EventHandlerService {

	private static final Logger LOG = Logger.getLogger(EventHandlerService.class.getName());

	private final Map<String, EventRecord> recordMap;

	private Executor executors;

	@Autowired
	private RestTemplate restTemplate;

	public EventHandlerService() {
		recordMap = new ConcurrentHashMap<>();
		executors = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	}

	public Set<Event> getByUrl(String url) {
		return recordMap.values().stream().filter(r -> r.hasSubscriber(url)).map(EventRecord::getEvent)
				.collect(Collectors.toSet());
	}

	public ResponseEvent notify(RequestEvent event) {
		if (event == null || event.getEventType() == null) {
			return new ResponseEvent(false);
		}
		EventRecord record = getEventRecord(event.getEventType());
		int count = forwardEventsAsync(event, record.getSubscribersMap().values());
		LOG.info(() -> "Forwarded : " + count + "/" + record.getSubscribersMap().size());
		return new ResponseEvent(true);
	}

	private int forwardEventsAsync(RequestEvent event, Collection<SubscribeEvent> subscribers) {
		subscribers.forEach(el -> LOG.severe(el.toString()));
		return (int) subscribers.stream().map(s -> forwardEventAsync(event, s)).map(CompletableFuture::join)
				.filter(Boolean::booleanValue).count();
	}

	private CompletableFuture<Boolean> forwardEventAsync(RequestEvent event, SubscribeEvent subscribeEvent) {
		return CompletableFuture.supplyAsync(() -> forwardEvent(event, subscribeEvent), executors).exceptionally(ex -> {
			LOG.severe(ex.getClass().getSimpleName() + " : " + ex.getMessage());
			return false;
		});
	}

	private boolean forwardEvent(RequestEvent event, SubscribeEvent eventSub) {
		final String url = eventSub.getUrl();
		LOG.info(() -> "Forwarding notify event " + event.getEventType() + " to " + url);
		try {
			ResponseEvent responseEvent = restTemplate.postForObject(url, event, ResponseEvent.class);

			if (responseEvent == null) {
				return false;
			}
			return responseEvent.isSuccess();
		} catch (Exception e) {
			LOG.severe(e.getClass().getSimpleName() + " : " + e.getMessage());
		}
		return false;
	}

	public ResponseEvent subscribe(SubscribeEvent event) {
		if (event == null || event.getEventType() == null) {
			return new ResponseEvent(false);
		}
		final EventRecord record = getEventRecord(event.getEventType());
		return record.addSubscriber(event) ? new ResponseEvent(true) : new ResponseEvent(false);
	}

	public ResponseEvent subscribe(RequestEvent event) {
		SubscribeEvent eventSub = new SubscribeEvent(event);
		return subscribe(eventSub);
	}

	public Set<URL> getSubscribers(String eventType) {
		if (recordMap.containsKey(eventType)) {
			Collection<SubscribeEvent> events = recordMap.get(eventType).getSubscribersMap().values();
			return events.stream().map(r -> RESTUtils.parseUrl(r.getUrl())).filter(Optional::isPresent)
					.map(Optional::get).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

	public ResponseEvent unsubscribe(SubscribeEvent event) {
		if (event == null || event.getEventType() == null) {
			LOG.info("Unscribing failed");
			return new ResponseEvent(false);
		}
		final EventRecord record = recordMap.get(event.getEventType());
		LOG.info("Unscribing " + event.getEventType());
		return record.removeSubscriber(event) ? new ResponseEvent(true) : new ResponseEvent(false);
	}

	private EventRecord getEventRecord(String eventType) {
		if (!recordMap.containsKey(eventType)) {
			LOG.info(() -> "Subscribing -> creating eventType : " + eventType);
			recordMap.put(eventType, new EventRecord(eventType));
		}
		LOG.info(() -> "Subscribing -> getting eventType : " + eventType);
		return recordMap.get(eventType);
	}

}
