package it.cnr.iit.eventhandler.deployer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan("it.cnr.iit.eventhandler")
public class EvenHandlerDeployer extends SpringBootServletInitializer {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder
				// .basicAuthentication(properties.getUsername(), properties.getPassword())
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(EvenHandlerDeployer.class, args);
	}

}
