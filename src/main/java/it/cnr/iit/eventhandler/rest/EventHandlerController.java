package it.cnr.iit.eventhandler.rest;

import java.net.URL;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.SubscribeEvent;
import it.cnr.iit.eventhandler.EventHandlerService;

@ApiModel(value = "EventHandler", description = "event handler")
@RestController
public class EventHandlerController {

	@Autowired
	private EventHandlerService eventHandler;

	@GetMapping(EventHandlerRestOperations.EVENTS)
	public Set<Event> getEvents(@RequestParam String subscriptionUrl) {
		return eventHandler.getByUrl(subscriptionUrl);
	}

	@PostMapping(EventHandlerRestOperations.NOTIFY_EVENT)
	public ResponseEvent notifyEvent(@RequestBody RequestEvent event) {
		return eventHandler.notify(event);
	}

	@PostMapping(EventHandlerRestOperations.SUBSCRIBE)
	public ResponseEvent subscribeEvent(@RequestBody SubscribeEvent event) {
		return eventHandler.subscribe(event);
	}

	@PostMapping(EventHandlerRestOperations.SUBSCRIBE_EV)
	public ResponseEvent subscribeEvent(@RequestBody RequestEvent event) {
		return eventHandler.subscribe(event);
	}

	@GetMapping(EventHandlerRestOperations.SUBSCRIBERS)
	public Set<URL> subscribers(@RequestParam String eventType) {
		return eventHandler.getSubscribers(eventType);
	}

	@PostMapping(EventHandlerRestOperations.UNSUBSCRIBE)
	public ResponseEvent unsubscribeEvent(@RequestBody SubscribeEvent event) {
		return eventHandler.unsubscribe(event);
	}

}
